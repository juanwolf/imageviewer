    /*
 * ImageViewer is a library which give the possibility
 * to the user to have a better render of the image.
 * This file is under CC BY-SA 4.0
 */

var Image = {
    /**
     * Define the image object by the specific id.
     */
    init: function(id) {},
    
    setTitle: function() {},
    
    setDescription : function() {},
    
    /**
     * Detect the orientation if the picture is portait oriented.
     */
    isPortaitImage : function() {},
};

var ImageViewer = {
    
    /* 
     * Create a image viewer specific to an image specified by the id.
     */
    createImageViewerFor: function(id) {},
    
    /**
     * Create a default image viewer for all images in the html document.
     */
    createDefaultImageViewer: function() {},
    
    /* 
     * Set the background color of the ImagViewer.
     */
    setBackgroundColorViewer: function(color) {},
    
    /*
     * Increase the size of the image. It will take the percentage of the screen.
     */
    increaseImageSize: function(percentage) {},
    
    /**
     * Define the controller for this image viewer for a specific image specified by the id.
     */
    createControllerFor: function(id) {},
};